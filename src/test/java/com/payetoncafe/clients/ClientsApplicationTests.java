package com.payetoncafe.clients;

import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.mockStatic;

@SpringBootTest
class ClientsApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void testMainMethod() {
		try (var mockedSpringApplication = mockStatic(SpringApplication.class)) {
			ClientsApplication.main(new String[]{});
			mockedSpringApplication.verify(() -> SpringApplication.run(ClientsApplication.class, new String[]{}));
		}
	}
}