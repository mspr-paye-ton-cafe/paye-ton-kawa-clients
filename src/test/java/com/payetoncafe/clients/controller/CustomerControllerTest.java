package com.payetoncafe.clients.controller;

import com.payetoncafe.clients.api.controller.CustomerController;
import com.payetoncafe.clients.exception.CustomerException;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @InjectMocks
    CustomerController customerController;
    @Mock
    CustomerServiceImpl customerService;

    @Test
    public void getAllCustomersTest() {
        Customer customer = initCustomer(1);
        Customer customer1 = initCustomer(2);
        List<Customer> customers = new ArrayList<>();
        customers.add(customer);
        customers.add(customer1);

        Mockito.when(customerService.getAllCustomers()).thenReturn(customers);

        ResponseEntity<List<Customer>> response = customerController.getAllCustomers();

        assertEquals(2, Objects.requireNonNull(response.getBody()).size());
        assertEquals(200, response.getStatusCode().value());
    }

    @Test
    public void getCustomerByIdTest() throws CustomerException {
        Customer customer = initCustomer(3);
        Mockito.when(customerService.getCustomerById(3)).thenReturn(customer);

        ResponseEntity<Customer> response = customerController.getCustomerById(3);

        assertEquals(customer, response.getBody());
        assertEquals(200, response.getStatusCode().value());
    }

    @Test
    public void getCustomerByIdTestKO() throws CustomerException {
        Mockito.when(customerService.getCustomerById(0)).thenThrow(new CustomerException("KO"));

        ResponseStatusException customerException = assertThrows(ResponseStatusException.class, () -> customerController.getCustomerById(0));

        assertEquals(404, customerException.getStatusCode().value());
    }

    @Test
    public void createCustomerTest() throws CustomerException {
        Customer customer = initCustomer(4);
        Mockito.when(customerService.createCustomer(customer)).thenReturn(customer);

        ResponseEntity<Customer> response = customerController.createCustomer(customer);

        Mockito.verify(customerService).createCustomer(customer);
        assertEquals(ResponseEntity.created(URI.create("/customers/" + customer.getId())).body(customer), response);
    }

    @Test
    public void createCustomerTestKO() throws CustomerException {
        Customer customer = initCustomer(4);
        customer.setUsername(null);
        doThrow(new CustomerException("Erreur lors de la création du client suivant : null")).when(customerService).createCustomer(customer);

        ResponseStatusException customerException = assertThrows(ResponseStatusException.class, () -> customerController.createCustomer(customer));

        assertEquals(HttpStatus.BAD_REQUEST, customerException.getStatusCode());
        assertEquals("Erreur lors de la création du client suivant : null", customerException.getReason());
        Mockito.verify(customerService).createCustomer(customer);
    }

    @Test
    public void deleteCustomerTest() throws CustomerException {
        Mockito.doNothing().when(customerService).deleteCustomer(anyInt());

        ResponseEntity<Void> response = customerController.deleteCustomer(1);

        Mockito.verify(customerService).deleteCustomer(1);
        assertEquals(ResponseEntity.ok().build(), response);
    }

    @Test
    public void deleteCustomerTestKO() throws CustomerException {
        doThrow(new CustomerException("Impossible de trouver le client avec l'id 0")).when(customerService).deleteCustomer(0);

        ResponseStatusException customerException = assertThrows(ResponseStatusException.class, () -> customerController.deleteCustomer(0));

        assertEquals(HttpStatus.NOT_FOUND, customerException.getStatusCode());
        assertEquals("Impossible de trouver le client avec l'id 0", customerException.getReason());
        Mockito.verify(customerService).deleteCustomer(0);
    }

    @Test
    public void updateCustomerTest() throws CustomerException {
        Customer customer = initCustomer(5);
        Mockito.when(customerService.updateCustomer(customer)).thenReturn(customer);

        ResponseEntity<Customer> response =  customerController.updateCustomer(5, customer);

        Mockito.verify(customerService).updateCustomer(customer);
        assertEquals(ResponseEntity.ok(customer), response);
    }

    @Test
    public void updateCustomerTestKO() throws CustomerException {
        Customer customer = initCustomer(5);
        doThrow(new CustomerException("Erreur lors de la modification du client avec l'ID 5 : Impossible de trouver le client avec l'id ")).when(customerService).updateCustomer(customer);

        ResponseStatusException customerException = assertThrows(ResponseStatusException.class, () -> customerController.updateCustomer(5, customer));

        assertEquals(HttpStatus.NOT_MODIFIED, customerException.getStatusCode());
        assertTrue(customerException.getReason().contains("Erreur lors de la modification du client avec l'ID 5"));
        Mockito.verify(customerService).updateCustomer(customer);
    }

    public Customer initCustomer(Integer id) {
        Customer customer = new Customer(OffsetDateTime.of(
                2024, 5, 15, 12, 30, 0, 0, ZoneOffset.UTC),
                "name", "username", "firstname", "lastname",
                new Address("11111","city"), new Company("company"));
        customer.setId(id);
        return customer;
    }
}
