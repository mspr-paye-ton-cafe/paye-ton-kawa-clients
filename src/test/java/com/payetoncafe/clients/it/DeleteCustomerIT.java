package com.payetoncafe.clients.it;

import com.payetoncafe.clients.events.CustomerProducer;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DeleteCustomerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    @MockBean
    private CustomerProducer customerProducer;

    @Test
    void deleteCustomerAPINotFound() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/customers/{id}", 0))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void deleteCustomerAPI() throws Exception {
        Customer customerToDelete = initCustomer();
        Customer customerCreated = customerRepository.save(customerToDelete);
        Integer customerId = customerCreated.getId();

        Mockito.doNothing().when(customerProducer).sendCustomerIdEvent(anyInt());

        mockMvc.perform(MockMvcRequestBuilders.delete("/customers/{id}", customerId))
                .andExpect(status().isOk());

        boolean exists = customerRepository.existsById(customerId);
        assertThat(exists).isFalse();
        Mockito.verify(customerProducer, Mockito.times(1)).sendCustomerIdEvent(customerId);
    }

    public Customer initCustomer() {
        Customer customer = new Customer(OffsetDateTime.of(
                2024, 5, 15, 12, 30, 0, 0, ZoneOffset.UTC),
                "name", "username", "firstname", "lastname",
                new Address("11111","city"), new Company("company"));
        return customer;
    }
}
