package com.payetoncafe.clients.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PostCustomerIT {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void createCustomerAPI() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/customers")
                        .content(asJsonString(initCustomer()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    @Test
    public void createCustomerByIdAPIKo() throws Exception {
        Customer customerWithError = initCustomer();
        customerWithError.setUsername(null);
        mockMvc.perform(MockMvcRequestBuilders.post("/customers")
                        .content(asJsonString(customerWithError))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Customer initCustomer() {
        return new Customer(OffsetDateTime.of(
                2024, 5, 15, 12, 30, 0, 0, ZoneOffset.UTC),
                "name", "username", "firstname", "lastname",
                new Address("11111","city"), new Company("company"));
    }

}
