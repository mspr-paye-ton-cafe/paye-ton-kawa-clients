package com.payetoncafe.clients.it;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PutCustomerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Test
    @Transactional
    void updateCustomerAPI() throws Exception {
        Customer customerToUpdate = initCustomer();
        Customer customer = customerRepository.save(customerToUpdate);

        customer.setUsername("newUsername");
        customer.setFirstName("newFirstName");
        customer.setLastName("newLastName");

        customer.getAddress().setCity("newCity");
        customer.getCompany().setCompanyName("newCompanyName");


        mockMvc.perform(MockMvcRequestBuilders
                        .put("/customers/{id}", customer.getId())
                        .content(asJsonString(customer))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value("newUsername"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("newFirstName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("newLastName"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.postalCode").value("11111"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.address.city").value("newCity"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.company.companyName").value("newCompanyName"));

    }

    @Test
    void updateCustomerAPIKo() throws Exception {
        Customer customer = initCustomer();
        customer.setId(0);

        mockMvc.perform( MockMvcRequestBuilders
                        .put("/customers/{id}", 0)
                        .content(asJsonString(customer))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotModified());
    }


    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            return objectMapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Customer initCustomer() {
        return new Customer(OffsetDateTime.of(
                2024, 5, 15, 12, 30, 0, 0, ZoneOffset.UTC),
                "name", "username", "firstname", "lastname",
                new Address("11111","city"), new Company("company"));
    }
}
