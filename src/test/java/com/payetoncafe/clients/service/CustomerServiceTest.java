package com.payetoncafe.clients.service;

import com.payetoncafe.clients.events.CustomerProducer;
import com.payetoncafe.clients.exception.CustomerException;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.repository.CustomerRepository;
import com.payetoncafe.clients.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @InjectMocks
    CustomerServiceImpl customerService;

    @Mock
    CustomerRepository customerRepository;

    @Mock
    CustomerProducer customerProducer;

    @Test
    public void getAllCustomersTest() {
        List<Customer> customers = new ArrayList<>();
        Customer customer = initCustomer(1);
        Customer customer1 = initCustomer(2);

        customers.add(customer);
        customers.add(customer1);

        Mockito.when(customerRepository.findAll()).thenReturn(customers);

        List<Customer> customerList = customerService.getAllCustomers();

        assertEquals(customers.size(), customerList.size());
    }

    @Test
    public void getCustomerByIdTest() throws CustomerException {
        Customer customer = initCustomer(1);

        Mockito.when(customerRepository.findById(1)).thenReturn(Optional.of(customer));

        Customer ReturnedCustomer = customerService.getCustomerById(1);

        assertEquals(ReturnedCustomer.getCreatedAt(), customer.getCreatedAt());
        assertEquals(ReturnedCustomer.getName(), customer.getName());
        assertEquals(ReturnedCustomer.getUsername(), customer.getUsername());
        assertEquals(ReturnedCustomer.getFirstName(), customer.getFirstName());
        assertEquals(ReturnedCustomer.getLastName(), customer.getLastName());
        assertEquals(ReturnedCustomer.getCompany().getCompanyName(), customer.getCompany().getCompanyName());
        assertEquals(ReturnedCustomer.getAddress().getPostalCode(), customer.getAddress().getPostalCode());
        assertEquals(ReturnedCustomer.getAddress().getCity(), customer.getAddress().getCity());
    }

    @Test
    public void getCustomerByIdTestKO() {
        Optional<Customer> customerOptional = Optional.empty();

        Mockito.when(customerRepository.findById(0)).thenReturn(customerOptional);

        CustomerException exception = assertThrows(CustomerException.class, () ->
                customerService.getCustomerById(0)
        );

        String expectedMessage = "Impossible de trouver le client";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void createCustomerTest() throws CustomerException {
        Customer customer = initCustomer(3);
        Mockito.when(customerRepository.save(customer)).thenReturn(customer);

        Customer createdCustomer = customerService.createCustomer(customer);

        assertEquals(createdCustomer.getCreatedAt(), customer.getCreatedAt());
        assertEquals(createdCustomer.getName(), customer.getName());
        assertEquals(createdCustomer.getUsername(), customer.getUsername());
        assertEquals(createdCustomer.getFirstName(), customer.getFirstName());
        assertEquals(createdCustomer.getLastName(), customer.getLastName());
        assertEquals(createdCustomer.getCompany().getCompanyName(), customer.getCompany().getCompanyName());
        assertEquals(createdCustomer.getAddress().getPostalCode(), customer.getAddress().getPostalCode());
        assertEquals(createdCustomer.getAddress().getCity(), customer.getAddress().getCity());
    }

    @Test
    public void createCustomerTestKO() {
        Customer customer = initCustomer(4);
        customer.setUsername(null);

        CustomerException customerException = assertThrows(CustomerException.class, () -> customerService.createCustomer(customer));

        assertTrue(customerException.getMessage().contains("Impossible de créer le client car un paramètre est manquant"));
    }

    @Test
    public void deleteCustomer() throws CustomerException {
        Integer customerId = 5;
        Customer customer = initCustomer(customerId);
        Mockito.when(customerRepository.findById((customerId))).thenReturn(Optional.of(customer));
        doNothing().when(customerProducer).sendCustomerIdEvent(customerId);

        customerService.deleteCustomer(customerId);

        Mockito.verify(customerRepository).deleteById(customerId);
        Mockito.verify(customerRepository).findById(customerId);
        Mockito.verify(customerProducer).sendCustomerIdEvent(customerId);
    }

    @Test
    void deleteCustomerKO() {
        Integer customerId = 6;
        Mockito.when(customerRepository.findById(customerId)).thenReturn(Optional.empty());

        CustomerException customerException = assertThrows(CustomerException.class, () -> customerService.deleteCustomer(customerId));

        assertTrue(customerException.getMessage().contains("Impossible de trouver le client avec l'id 6"));
    }

    public Customer initCustomer(Integer id) {
        Customer customer = new Customer(OffsetDateTime.of(
                2024, 5, 15, 12, 30, 0, 0, ZoneOffset.UTC),
                "name", "username", "firstname", "lastname",
                new Address("11111","city"), new Company("company"));
        customer.setId(id);
        return customer;
    }

}