package com.payetoncafe.clients.events;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class CustomerProducerTest {
        @Mock
        private KafkaTemplate<String, Integer> kafkaTemplate;

        @InjectMocks
        private CustomerProducer customerProducer;

        @Test
        public void sendCustomerIdEventTest() {
            // Given
            Integer productId = 1;
            // When
            customerProducer.sendCustomerIdEvent(productId);
            // Then
            verify(kafkaTemplate, times(1)).send(CustomerProducer.TOPIC, productId);
        }
}
