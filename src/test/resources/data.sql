-- Insert Addresses
INSERT INTO address (postal_code, city) VALUES ('12345', 'New York');
INSERT INTO address (postal_code, city) VALUES ('54321', 'Los Angeles');
INSERT INTO address (postal_code, city) VALUES ('67890', 'Chicago');
INSERT INTO address (postal_code, city) VALUES ('98765', 'Houston');
INSERT INTO address (postal_code, city) VALUES ('13579', 'Philadelphia');

-- Insert Companies
INSERT INTO company (company_name) VALUES ('TechSolutions');
INSERT INTO company (company_name) VALUES ('InnovativeTech');
INSERT INTO company (company_name) VALUES ('Global Enterprises');
INSERT INTO company (company_name) VALUES ('SmartServices');
INSERT INTO company (company_name) VALUES ('Dynamic Solutions');

-- Insert Customers
INSERT INTO customer (created_at, name, username, first_name, last_name, address_id, company_id) VALUES ('2024-05-15T00:00:000', 'John Doe', 'johndoe', 'John', 'Doe', 1, 1);
INSERT INTO customer (created_at, name, username, first_name, last_name, address_id, company_id) VALUES ('2024-05-15T00:00:000', 'Jane Smith', 'janesmith', 'Jane', 'Smith', 2, 2);
INSERT INTO customer (created_at, name, username, first_name, last_name, address_id, company_id) VALUES ('2024-05-15T00:00:000', 'Alice Johnson', 'alicejohnson', 'Alice', 'Johnson', 3, 3);
INSERT INTO customer (created_at, name, username, first_name, last_name, address_id, company_id) VALUES ('2024-05-15T00:00:000', 'Bob Brown', 'bobbrown', 'Bob', 'Brown', 4, 4);
INSERT INTO customer (created_at, name, username, first_name, last_name, address_id, company_id) VALUES ('2024-05-15T00:00:000', 'Michael Wilson', 'michaelwilson', 'Michael', 'Wilson', 5, 5);
