package com.payetoncafe.clients.api.controller;

import com.payetoncafe.clients.api.CustomersApi;
import com.payetoncafe.clients.exception.CustomerException;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.model.UpdateCustomerAttributeRequest;
import com.payetoncafe.clients.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
public class CustomerController implements CustomersApi {

    private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseEntity<Customer> createCustomer(Customer customer) {
        try {
            Customer createdCustomer = this.customerService.createCustomer(customer);
            log.info("Création avec succès du client {}", createdCustomer.getUsername());

            return ResponseEntity
                    .created(URI.create("/customers/" + createdCustomer.getId()))
                    .body(createdCustomer);
        } catch (CustomerException e) {
            log.error("Erreur lors de la création du client suivant : {}, {}", customer.getUsername(), e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


    @Override
    public ResponseEntity<Void> deleteCustomer(Integer customerId) {
            try {
                this.customerService.deleteCustomer(customerId);
                log.info("Suppression du client {}", customerId);
                return ResponseEntity.ok().build();
            } catch (CustomerException e) {
                log.info("Erreur lors de la suppression du client suivant : {}, {}", customerId, e.getMessage());
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
    }

    @Override
    public ResponseEntity<List<Customer>> getAllCustomers() {
        try {
            List<Customer> customers = customerService.getAllCustomers();
            log.info("Récupération avec succès de {} client(s)", customers.size());
            return ResponseEntity.ok(customers);
        } catch (Exception e) {
            log.error("Erreur lors de la récupération des clients : ", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Customer> getCustomerById(Integer customerId) {
        try {
            Customer customer = this.customerService.getCustomerById(customerId);
            log.info("Récupération avec succès du client avec l'ID {}", customerId);
            return ResponseEntity.ok(customer);
        } catch (CustomerException e) {
            log.error("Erreur lors de la récupération du client avec l'ID {} : {}", customerId, e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Customer> updateCustomer(Integer customerId, Customer customer) {
        try {
            Customer updatedCustomer = this.customerService.updateCustomer(customer);
            log.info("Modification avec succès du client {}", updatedCustomer.getUsername());
            return ResponseEntity.ok(updatedCustomer);
        } catch (CustomerException e) {
            log.error("Erreur lors de la modification du client avec l'ID {} : {}", customerId, e.getMessage());
            throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, e.getMessage());
        }
    }

    @Override
    public ResponseEntity<Void> updateCustomerAttribute(Integer customerId, UpdateCustomerAttributeRequest updateCustomerAttributeRequest) {
        return null;
    }
}
