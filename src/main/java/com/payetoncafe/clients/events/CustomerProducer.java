package com.payetoncafe.clients.events;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;


@Service
public class CustomerProducer {
    private static final Logger log = LoggerFactory.getLogger(CustomerProducer.class);

    public static final String TOPIC = "customerDelete";

    private final KafkaTemplate<String, Integer> kafkaTemplate;

    public CustomerProducer(KafkaTemplate<String, Integer> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendCustomerIdEvent(Integer customerId){
        log.info("Kafka : Début Envoi du client supprimé pour le supprimer dans commandes");
        kafkaTemplate.send(TOPIC, customerId);
        log.info("Kafka : Fin Envoi du client à supprimer");
    }
}
