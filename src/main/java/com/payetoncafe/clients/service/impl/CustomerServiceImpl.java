package com.payetoncafe.clients.service.impl;

import com.payetoncafe.clients.events.CustomerProducer;
import com.payetoncafe.clients.exception.CustomerException;
import com.payetoncafe.clients.model.Address;
import com.payetoncafe.clients.model.Company;
import com.payetoncafe.clients.model.Customer;
import com.payetoncafe.clients.repository.CustomerRepository;
import com.payetoncafe.clients.service.CustomerService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final CustomerRepository customerRepository;

    private final CustomerProducer customerProducer;

    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerProducer customerProducer) {
        this.customerRepository = customerRepository;
        this.customerProducer = customerProducer;
    }

    @Override
    public Customer getCustomerById(Integer customerId) throws CustomerException {
        return this.customerRepository.findById(customerId).orElseThrow(() -> new CustomerException("Impossible de trouver le client avec l'id " + customerId));
    }

    @Override
    public List<Customer> getAllCustomers() {
        return this.customerRepository.findAll();
    }

    @Override
    public Customer createCustomer(Customer customer) throws CustomerException {
        Customer customerToCreate;
        if (customer.getUsername() != null) {
            customerToCreate = this.customerRepository.save(customer);
            log.info("Ajout en base réussi : {}", customerToCreate);
        } else {
            log.error("Impossible de créer le client car un paramètre est manquant");
            throw new CustomerException("Impossible de créer le client car un paramètre est manquant");
        }
        return customerToCreate;
    }

    @Override
    public Customer updateCustomer(Customer customer) throws CustomerException {
        Customer customerToUpdate = this.customerRepository.findById(customer.getId()).orElseThrow(() -> new CustomerException("Impossible de trouver le client avec l'id " + customer.getId()));

        customerToUpdate.setUsername(customer.getUsername());
        customerToUpdate.setFirstName(customer.getFirstName());
        customerToUpdate.setLastName(customer.getLastName());
        customerToUpdate.setName(customer.getName());

        Address addressToUpdate = customerToUpdate.getAddress();
        addressToUpdate.setCity(customer.getAddress().getCity());
        addressToUpdate.setPostalCode(customer.getAddress().getPostalCode());
        customerToUpdate.setAddress(addressToUpdate);
        log.info(String.valueOf(customerToUpdate.getAddress()));

        Company companyToUpdate = customerToUpdate.getCompany();
        companyToUpdate.setCompanyName(customer.getCompany().getCompanyName());
        customerToUpdate.setCompany(companyToUpdate);
        log.info(String.valueOf(customerToUpdate.getCompany()));

        return customerRepository.save(customerToUpdate);
    }

    @Override
    @Transactional
    public void deleteCustomer(Integer customerId) throws CustomerException {
        Customer customerToDelete = getCustomerById(customerId);
        this.customerRepository.deleteById(customerId);
        log.info("Suppression en base réussie : {}", customerToDelete);
        this.customerProducer.sendCustomerIdEvent(customerId);
    }
}
