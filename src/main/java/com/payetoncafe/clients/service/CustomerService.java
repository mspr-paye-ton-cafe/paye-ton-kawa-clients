package com.payetoncafe.clients.service;

import com.payetoncafe.clients.exception.CustomerException;
import com.payetoncafe.clients.model.Customer;

import java.util.List;

public interface CustomerService{

    Customer getCustomerById(Integer customerId) throws CustomerException;

    List<Customer> getAllCustomers();

    Customer createCustomer(Customer customer) throws CustomerException;

    Customer updateCustomer(Customer customer) throws CustomerException;

    void deleteCustomer(Integer customerId) throws CustomerException;
}
