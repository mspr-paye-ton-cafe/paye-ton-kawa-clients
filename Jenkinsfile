pipeline {
    agent any

    environment {
        PATH = "${JAVA_HOME}/bin:${PATH}"
        SONAR_USER_HOME = "${WORKSPACE}/.sonar"
        MAVEN_OPTS = '-Dmaven.repo.local=.m2/repository'
        GIT_DEPTH = "0"
        SONAR_HOST_URL = credentials('sonar-host-url')
        SONAR_TOKEN = credentials('sonar-token')
        NVD_API_KEY = credentials('nvd-api-key')
        DOCKER_REGISTRY = 'index.docker.io/v1' 
        DOCKER_CREDENTIALS_ID = credentials('dockerhub-credentials')
        DOCKERHUB_USERNAME = 'mspr123'
    }

    tools {
        jdk 'openjdk21'
        maven 'Maven'
    }

    stages {
        stage('Checkout') {
            steps {
                script {
                    echo "Checking out branch: ${env.BRANCH_NAME}"
                    checkout([$class: 'GitSCM', branches: [[name: "${env.BRANCH_NAME}"]], userRemoteConfigs: [[url: 'https://gitlab.com/mspr-paye-ton-cafe/paye-ton-kawa-clients.git']]])
                    // Get commit message and set build description
                    def commitMessage = sh(script: "git log -1 --pretty=%B", returnStdout: true).trim()
                    def branchName = sh(script: "git rev-parse --abbrev-ref HEAD", returnStdout: true).trim()
                    currentBuild.description = "${commitMessage} - ${branchName}"
                }
            }
        }

        stage('Print Workspace Structure') {
            steps {
                script {
                    sh 'ls -la'
                }
            }
        }

        stage('Setup') {
            steps {
                script {
                    env.JAVA_OPTS = '-Xmx1024m -Xms512m'
                    echo "JAVA_HOME is set to: ${env.JAVA_HOME}"
                }
            }
        }

        stage('Check') {
            steps {
                sh 'mvn clean'
                sh 'mvn validate'
            }
        }

        stage('Build') {
            steps {
                sh 'mvn compile'
            }
        }

        stage('Unit Test') {
            steps {
                sh 'mvn test'
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                    archiveArtifacts artifacts: 'target/surefire-reports/*.xml', allowEmptyArchive: true
                }
            }
        }

        stage('Integration Test') {
            steps {
                sh 'mvn verify -DskipUnitTests=true'
            }
            post {
                always {
                    junit 'target/failsafe-reports/*.xml'
                    archiveArtifacts artifacts: 'target/failsafe-reports/*.xml', allowEmptyArchive: true
                }
            }
        }

        stage('SonarCloud Analysis') {
            steps {
                withSonarQubeEnv('SonarQube') {
                    sh """
                        mvn verify sonar:sonar \
                            -Dsonar.projectKey=mspr-paye-ton-cafe_paye-ton-kawa-clients \
                            -Dsonar.organization=mspr-paye-ton-cafe \
                            -Dsonar.host.url=${env.SONAR_HOST_URL} \
                            -Dsonar.login=${env.SONAR_TOKEN} \
                            -Dsonar.branch.name=${env.BRANCH_NAME}
                    """
                }
            }
        }

        stage('Build Docker Image') {
            steps {
                script {
                    def dockerImage = docker.build("clients_app:${env.BRANCH_NAME}-${env.BUILD_NUMBER}", "-f ${WORKSPACE}/Dockerfile .")
                }
            }
        }


        stage('Package') {
            steps {
                sh 'mvn package -DskipTests'
            }
            post {
                success {
                    archiveArtifacts artifacts: 'target/*.jar', allowEmptyArchive: true
                }
            }
        }
    }
}